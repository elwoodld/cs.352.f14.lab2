# Variables
CC	 = gcc
CFLAGS	 = -Wall -std=gnu99 -g -I$(shell pwd)/library
LDFLAGS	 = -L$(shell pwd)

LIBRARIES= library/adder.c \
	   library/adder16.c \
	   library/alu16.c \
	   library/clock.c \
	   library/counter16.c \
	   library/dff.c \
	   library/logic.c \
	   library/logic16.c \
	   library/memory16.c \
	   library/register.c \
	   library/register16.c \
	   library/util.c
LIBRARY_OBJECTS	= $(LIBRARIES:.c=.o)
LIBRARY_HEADER	= library/sam16.h

PROGRAMS = programs/alu \
	   programs/timer \
	   programs/tt
PROGRAM_OBJECTS = $(PROGRAMS:=.o)

TESTS    = tests/test_adder \
	   tests/test_adder16 \
	   tests/test_alu16 \
	   tests/test_clock \
	   tests/test_counter16 \
	   tests/test_dff \
	   tests/test_logic \
	   tests/test_logic16 \
	   tests/test_memory16 \
	   tests/test_register \
	   tests/test_register16 \
	   tests/test_util
TEST_OBJECTS = $(TESTS:=.o)

LIBRARY	 = library/libsam16.a

# Rules

all:	$(LIBRARY) $(TESTS) $(PROGRAMS)

debug:  CFLAGS += -DDEBUG
debug:  $(LIBRARY) $(TESTS) $(PROGRAMS)

doc:	Doxyfile
	doxygen

%.o: %.c $(LIBRARY_HEADER)
	$(CC) $(CFLAGS) -o $@ -c $<

$(LIBRARY): $(LIBRARY_OBJECTS)
	$(AR) rcs $(LIBRARY) $(LIBRARY_OBJECTS)

$(TESTS): %: %.o $(LIBRARY) $(LIBRARY_HEADER)
	$(CC) $(LDFLAGS) -o $@ $< $(LIBRARY) $(LIBS)

$(PROGRAMS): %: %.o $(LIBRARY) $(LIBRARY_HEADER)
	$(CC) $(LDFLAGS) -o $@ $< $(LIBRARY) $(LIBS)

clean:
	rm -f $(LIBRARY) $(PROGRAM_OBJECTS) $(TEST_OBJECTS) $(LIBRARY_OBJECTS) $(TESTS) $(PROGRAMS)

test:	$(TESTS)
	./tests/test_util 	| diff -u - ./tests/test_util.output
	./tests/test_logic 	| diff -u - ./tests/test_logic.output
	./tests/test_logic16 	| diff -u - ./tests/test_logic16.output
	./tests/test_adder	| diff -u - ./tests/test_adder.output
	./tests/test_adder16	| diff -u - ./tests/test_adder16.output
	./tests/test_alu16	| diff -u - ./tests/test_alu16.output
	./tests/test_clock	| diff -u - ./tests/test_clock.output
	./tests/test_dff	| diff -u - ./tests/test_dff.output
	./tests/test_register	| diff -u - ./tests/test_register.output
	./tests/test_register16	| diff -u - ./tests/test_register16.output
	./tests/test_memory16	| diff -u - ./tests/test_memory16.output
	./tests/test_counter16  | diff -u - ./tests/test_counter16.output
