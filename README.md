CS.352.F13 - Lab 2: Logic Design
================================

[Lab 2: Logic Design](http://cs.uwec.edu/~buipj/teaching/cs.352.f14/lab_02_logic_design.html)

Group Members
-------------
- Lucas Elwood <elwoodld@uwec.edu>
- Peter Bui <buipj@uwec.edu>

Problems/Questions/Comments?
----------------------------

Please list any problems, questions, or comments about this assignment or the
class here.
