/* adder.c: Adder functions */

#include "sam16.h"

/**
 * Half Adder: s = sum, cout = carry out
 *
 * This computes the sum of two bits a and b and stores the result in s.  Any
 * carryover is stored in cout.
 *
 * @param a    Input bit.
 * @param b    Input bit.
 * @param s    Output bit.
 * @param cout Output bit.
 */
void HalfAdder(bit_t a, bit_t b, bit_t *s, bit_t *cout) {
    // TODO: Implement in terms of 1-bit gates
    
        Xor(a,b, s);
        And(a,b,cout);
    
}

/**
 * Full Adder: s = sum, cout = carry out
 *
 * This computes the sum of three bits a, b, and cin and stores the result in
 * s.  Any carryover is stored in cout.
 *
 * @param a    Input bit.
 * @param b    Input bit.
 * @param cin  Input bit.
 * @param s    Output bit.
 * @param cout Output bit.
 */
void FullAdder(bit_t a, bit_t b, bit_t cin, bit_t *s, bit_t *cout) {
    // TODO: Implement in terms of 1-bit gates
    Xor(a,b,s);
    Xor(*s,cin,s);
   
    bit_t ab;
    bit_t aXorB;

    And(a,b,&ab);
    Xor(a,b,&aXorB);
    And(aXorB,cin,cout);
    Or(*cout,ab,cout);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
