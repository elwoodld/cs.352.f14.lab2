/* alu16.c: 16-bit ALU functions */

#include "sam16.h"

/**
 * ALU16: out = f(x,y), zr = 1 iff out == 0, ng = 1 iff out < 0
 *
 * This implements a multi-function ALU using basic logic gates.
 *
 * @param x   Input word.
 * @param y   Input word.
 * @param ctl Input control bits.
 * @param out Output word.
 * @param zr  Output bit.
 * @param ng  Output bit.
 */
void ALU16(word_t x, word_t y, struct alu_ctl_t ctl, word_t *out, bit_t *zr, bit_t *ng) {
    // TODO: Modify x input
    word_t xHolder;
    word_t xNot;
    word_t xOutput;

    Mux2To116(x, 0, ctl.zx, &xHolder);
    Not16(xHolder,&xNot);
    Mux2To116(xHolder, xNot, ctl.nx, &xOutput);
    // TODO: Modify y input
    
    word_t yHolder;
    word_t yNot;
    word_t yOutput;

    Mux2To116(y, 0, ctl.zy, &yHolder);
    Not16(yHolder,&yNot);
    Mux2To116(yHolder, yNot, ctl.ny, &yOutput);

    // TODO: Compute ADD and AND functions

    word_t andOut;
    word_t addOut;
    bit_t cout;

    And16(xOutput,yOutput,&andOut);
    Adder16(xOutput,yOutput,0,&addOut, &cout);

    // TODO: Modify output
    word_t modOutput;
    word_t notModOutput;

    Mux2To116(andOut,addOut,ctl.f,&modOutput);
    Not16(modOutput,&notModOutput);
    Mux2To116(modOutput,notModOutput,ctl.no,out);
    // TODO: Detect negative
    
    *ng = GET_BIT(*out,WORD_SIZE-1);       
 
    // TODO: Detect zero
    Or16Way(*out,zr);
    Not(*zr,zr);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
