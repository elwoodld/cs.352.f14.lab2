/* clock.c: Clock functions */

#include "sam16.h"

/**
 * Global Clock variable.
 */
bit_t Clock = 0;

/**
 * Cycles global Clock.
 *
 * @return Previous Clock value.
 */
bit_t ClockCycle(void) {
    // TODO: Modify global clock and return previous value

    bit_t clk = Clock;
    if(Clock == 1){
        Clock = 0;
    }else{
        Clock = 1;
    }

    return clk;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
