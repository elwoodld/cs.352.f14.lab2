/* counter16.c: 16-bit Counter functions */

#include "sam16.h"

/**
 * Decoder that translates the three counter inputs into two output select bits:
 *
 *  s = 00  Reset
 *  s = 01  Load
 *  s = 10  Increment
 *  s = 11  Pass-through
 *
 * @param inc	Increment control bit.
 * @param load	Load control bit.
 * @param reset Control bit.
 * @param s     Output select bits.
 */
void Counter16Decoder(bit_t inc, bit_t load, bit_t reset, bit_t s[2]) {
    // TODO:
    //
    //	Construct a decoder such that the following is output based on the
    //	values of inc, load, and reset:
    //
    //	    00 Reset
    //	    01 Load
    //	    10 Increment
    //	    11 Pass-through
    bit_t not1;
    bit_t not2;
    bit_t not3;
    bit_t holder;
    bit_t holder2;
    bit_t holder3;  
    bit_t final;
    
    Not(reset,&not1);
    Not(load,&not2);
    And(not1,not2,&holder);
    s[1] = holder;

    Not(inc,&not3);
    And(not1,not3, &holder2);
    And(not1,load, &holder3);
    Or(holder2,holder3,&final);
    s[0] = final;
}

/**
 * Simulates a 16-bit counter.
 *
 * @param c	Internal counter state.
 * @param clk   Clock.
 * @param inc	Increment control bit.
 * @param load	Load control bit.
 * @param reset Control bit.
 * @param in	Input word.
 */
word_t Counter16Cycle(word_t c, bit_t clk, bit_t inc, bit_t load, bit_t reset, word_t in) {
    // TODO: Implement a counter cycle by using a Counter16Decoder,
    // Adder16, Mux4To116, and Register16Cycle.
    bit_t s[2];    
    word_t muxOut;
    word_t holder;
    bit_t out;
    

    Counter16Decoder(inc,load,reset,s);
    Adder16(1,c,0,&holder,&out);
    Mux4To116(0,in,holder,c,s,&muxOut);
    return Register16Cycle(c,clk,muxOut,1);
    //return c;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
