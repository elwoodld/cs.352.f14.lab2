/* dff.c: D Flip-Flop functions */

#include "sam16.h"

/**
 * Simulates an level-triggered D Flip-Flop cycle.
 *
 * @param d	Internal Flip-Flop state.
 * @param clk	Clock.
 * @param in	Input bit.
 */
bit_t DFFCycle(bit_t d, bit_t clk, bit_t in) {
    // TODO: When the clock goes from 0 to 1, then we store the input into the flip-flop
    if(clk == 1 && Clock == 0){
        d = in;
    }
    // TODO: The output is always the state of the flip-flop
    
    return d;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
