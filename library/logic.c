/* logic.c: Logic functions */

#include "sam16.h"

/**
 * NAND: f = a NAND b
 *
 * This implements a 1-bit NAND gate using C logical operators.
 *
 * @param a Input bit.
 * @param b Input bit.
 * @param f Output bit.
 */
void Nand(bit_t a, bit_t b, bit_t *f) {
    *f = !(a && b);
}

/**
 * NOT: f = NOT(a)
 *
 * This implements a 1-bit NOT gate using 1-bit NAND gates.
 *
 * @param a Input bit.
 * @param f Output bit.
 */
void Not(bit_t a, bit_t *f) {
    // TODO: Implement using only Nand
    Nand(a, a, f);
}

/**
 * AND: f = a AND b
 *
 * This implements a 1-bit AND gate using 1-bit NAND gates.
 *
 * @param a Input bit.
 * @param b Input bit.
 * @param f Output bit.
 */
void And(bit_t a, bit_t b, bit_t *f) {
    // TODO: Implement using only Nand
    bit_t aNandB;

    Nand(a, b, &aNandB);
    Nand(aNandB, aNandB, f);
}

/**
 * OR: f = a OR b
 *
 * This implements a 1-bit OR gate using 1-bit NAND gates.
 *
 * @param a Input bit.
 * @param b Input bit.
 * @param f Output bit.
 */
void Or(bit_t a, bit_t b, bit_t *f) {
    // TODO: Implement using only Nand
    bit_t aNandA;
    bit_t bNandB;
    
    Nand(a,a,&aNandA);
    Nand(b,b,&bNandB);
    Nand(aNandA,bNandB,f);
}

/**
 * XOR: f = a XOR b
 *
 * This implements a 1-bit XOR gate using 1-bit NAND gates.
 *
 * @param a Input bit.
 * @param b Input bit.
 * @param f Output bit.
 */
void Xor(bit_t a, bit_t b, bit_t *f) {
    // TODO: Implement using only Nand
    bit_t aNandB;
    bit_t aNand;
    bit_t bNand;

    Nand(a, b, &aNandB);
    Nand(a, aNandB, &aNand);
    Nand(b, aNandB, &bNand);
    Nand(aNand, bNand, f);
}

/**
 * 2to1 Mux: f = a if s == 0 else b
 *
 * This implements a 1-bit 2To1 Mux using 1-bit logical gates.
 *
 * @param a Input bit.
 * @param b Input bit.
 * @param s Select bit.
 * @param f Output bit.
 */
void Mux2To1(bit_t a, bit_t b, bit_t s, bit_t *f) {
    // TODO: Implement using And, Not, Or
    

    bit_t NotS;
    bit_t aAndNS;
    bit_t bAndS;

    Not(s,&NotS);
    And(a,NotS,&aAndNS);
    And(b,s,&bAndS);
    Or(aAndNS,bAndS, f);
    
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
