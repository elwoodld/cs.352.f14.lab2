/* logic16.c: 16-bit Logic functions */

#include "sam16.h"

/**
 * NAND16: f = a NAND b
 *
 * This implements a 16-bit NAND gate using 1-bit NAND gates.
 *
 * @param a Input word.
 * @param b Input word.
 * @param f Output word.
 */
void Nand16(word_t a, word_t b, word_t *f) {
    // TODO: Implement in terms of 1-bit equivalent
        
    bit_t holder;
    
    bit_t array[WORD_SIZE];
 
    for(int i = 0; i < WORD_SIZE; i++){
        Nand(GET_BIT(a,i), GET_BIT(b,i), &holder);
        array[i] = holder;
    }
    bits_to_word(array,f);
}

/**
 * NOT16: f = NOT(a)
 *
 * This implements a 16-bit NOT gate using 1-bit NOT gates.
 *
 * @param a Input word.
 * @param f Output word.
 */
void Not16(word_t a, word_t *f) {
    // TODO: Implement in terms of 1-bit equivalent
    bit_t holder;

    bit_t array[WORD_SIZE];
    
        for(int i = 0; i < WORD_SIZE; i++){
                Not(GET_BIT(a,i),&holder);
                array[i] = holder;
        }
        bits_to_word(array,f);
                                
}

/**
 * AND16: f = a AND b
 *
 * This implements a 16-bit AND gate using 1-bit AND gates.
 *
 * @param a Input word.
 * @param b Input word.
 * @param f Output word.
 */
void And16(word_t a, word_t b, word_t *f) {
    // TODO: Implement in terms of 1-bit equivalent
    
    bit_t holder;

    bit_t array[WORD_SIZE];

    for(int i = 0; i < WORD_SIZE; i++){
        And(GET_BIT(a,i), GET_BIT(b,i), &holder);
        array[i] = holder;
    }
    bits_to_word(array,f);

}

/**
 * OR16: f = a OR b
 *
 * This implements a 16-bit OR gate using 1-bit OR gates.
 *
 * @param a Input word.
 * @param b Input word.
 * @param f Output word.
 */
void Or16(word_t a, word_t b, word_t *f) {
    // TODO: implement in terms of 1-bit equivalent

    bit_t holder;

    bit_t array[WORD_SIZE];

    for(int i = 0; i < WORD_SIZE; i++){
        Or(GET_BIT(a,i), GET_BIT(b,i), &holder);
        array[i] = holder;
    }
    bits_to_word(array,f);


}

/**
 * OR16Way: f = w[0] OR w[1] OR ... OR w[15]
 *
 * This implements a 16-way OR gate using 1-bit OR gates.
 *
 * @param w Input word.
 * @param f Output bit.
 */
void Or16Way(word_t w, bit_t *f) {

    bit_t holder;
    bit_t val;

    for(int i = 0; i < WORD_SIZE; i++){
        Or(GET_BIT(w,i), GET_BIT(w,i), &holder);
        Or(holder,val, &val);
    }
    *f = val;


}

/**
 * XOR16: f = a XOR b
 *
 * This implements a 16-bit XOR gate using 1-bit XOR gates.
 *
 * @param a Input word.
 * @param b Input word.
 * @param f Output word.
 */
void Xor16(word_t a, word_t b, word_t *f) {
    // TODO: Implement in terms of 1-bit equivalent

    bit_t holder;

    bit_t array[WORD_SIZE];

    for(int i = 0; i < WORD_SIZE; i++){
        Xor(GET_BIT(a,i), GET_BIT(b,i), &holder);
        array[i] = holder;
    }
    bits_to_word(array,f);


}

/**
 * 2To1 Mux16: f = a if s == 0 else b
 *
 * This implements a 16-bit 2To1 Mux using 1-bit 2To1 Mux gates.
 *
 * @param a Input word.
 * @param b Input word.
 * @param s Select bit.
 * @param f Output word.
 */
void Mux2To116(word_t a, word_t b, bit_t s, word_t *f) {
    // TODO: Implement in terms of 1-bit equivalent
    
    bit_t holder;

    bit_t array[WORD_SIZE];

    for(int i = 0; i < WORD_SIZE; i++){
        Mux2To1(GET_BIT(a,i), GET_BIT(b,i),s, &holder);
        array[i] = holder;
    }
    bits_to_word(array,f);


}

/**
 * 4To1 Mux16: f = a if s == 0 else b
 *
 * This implements a 16-bit 4To1 Mux using 16-bit 2To1 Mux gates.
 *
 * @param a Input word.
 * @param b Input word.
 * @param c Input word.
 * @param d Input word.
 * @param s Select bits.
 * @param f Output word.
 */
void Mux4To116(word_t a, word_t b, word_t c, word_t d, bit_t s[2], word_t *f) {
    // TODO: Implement 4to1 16-bit mux using 3 Mux2To116's.
    bit_t ab;
    bit_t cd;
    bit_t abcd;

    bit_t array[WORD_SIZE];

   for(int i = 0; i < WORD_SIZE; i++){
        Mux2To1(GET_BIT(a,i), GET_BIT(b,i),s[0], &ab);
        Mux2To1(GET_BIT(c,i), GET_BIT(d,i),s[0], &cd);
        Mux2To1(ab, cd,s[1], &abcd);

        array[i] = abcd;
    }
    bits_to_word(array,f);


}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
