/* memory16.c: 16-bit memory functions */

#include "sam16.h"

/**
 * Simulates a 16-bit memory cycle.
 *
 * @param m	Memory word array.
 * @param clk	Clock.
 * @param in	Input word.
 * @param addr  Address word.
 * @param load  Load bit.
 */
word_t Memory16Cycle(word_t *m, bit_t clk, word_t in, word_t addr, bit_t load) {
    // TODO: Use Register16Cycle to implement a memory cycle.
    
    return Register16Cycle(m[addr],clk,in,load);
    //return m[addr];
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
