/* register.c: 1-bit register functions */

#include "sam16.h"

/**
 * Simulates a 1-bit register cycle.
 *
 * @param r	Register bit.
 * @param clk	Clock.
 * @param in	Input bit.
 * @param load  Load bit.
 */
bit_t RegisterCycle(bit_t r, bit_t clk, bit_t in, bit_t load) {
    // TODO: Use Mux2To1 and DFFCycle to implement a 1-bit register.
    
    bit_t muxOut;

    Mux2To1(r,in,load,&muxOut);
    return DFFCycle(r,clk,muxOut);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
