/* sam16.h: Simple 16-bit Machine */

#ifndef SAM16_H
#define SAM16_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

/* Constants */
#define WORD_SIZE   16       // TODO: Number of bits in a word
#define BS_LEN      WORD_SIZE + 1       // TODO: Number of chars in a binary string
#define UWORD_MAX   65535       // TODO: Maximum unsigned integer
#define WORD_MIN    -32768       // TODO: Lowest signed integer
#define WORD_MAX    32767      // TODO: Highest signed integer

typedef enum {
    ALU_AND,
    ALU_ADD,
} alu_func_t;

/* Type definitions */
typedef bool        bit_t;
typedef uint16_t    word_t;

struct alu_ctl_t {
    bit_t zx;               /**< Zero X input control bit. */
    bit_t nx;               /**< Not X input control bit. */
    bit_t zy;               /**< Zero Y input control bit. */
    bit_t ny;               /**< Not Y input control bit. */
    bit_t f;                /**< Function control bit. */
    bit_t no;               /**< Not output control bit. */
};

/* Macros */
#ifdef DEBUG
#define debug(M, ...) fprintf(stderr, "[DEBUG] %s:%d: " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#else
#define debug(M, ...)
#endif

#define warn(M, ...)  fprintf(stderr, "[WARN ] %s:%d: " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)

#define MIN(a, b)           ((a) < (b) ? (a) : (b))
#define MAX(a, b)           ((a) > (b) ? (a) : (b))

#define GET_BIT(w, n)       ((w >> (n)) & 1)
#define SET_BIT(w, n, v)    (w |= ((v) << (n)))
#define REV_BIT(n)          (WORD_SIZE - 1 - (n))

/* Global variables */
extern bit_t Clock;

/* Utility functions */
void word_to_binary_string(word_t w, char *s);
void binary_string_to_word(char *s, word_t *w);
void bits_to_word(bit_t b[WORD_SIZE], word_t *w);
void word_to_bits(word_t w, bit_t b[WORD_SIZE]);
void reverse_bits(bit_t b[WORD_SIZE]);

/* Logic functions */
void Nand(bit_t a, bit_t b, bit_t *f);
void Not(bit_t a, bit_t *f);
void And(bit_t a, bit_t b, bit_t *f);
void Or(bit_t a, bit_t b, bit_t *f);
void Xor(bit_t a, bit_t b, bit_t *f);
void Mux2To1(bit_t a, bit_t b, bit_t s, bit_t *f);

/* 16-bit Logic functions */
void Nand16(word_t a, word_t b, word_t *f);
void Not16(word_t a, word_t *f);
void And16(word_t a, word_t b, word_t *f);
void Or16(word_t a, word_t b, word_t *f);
void Or16Way(word_t w, bit_t *f);
void Xor16(word_t a, word_t b, word_t *f);
void Mux2To116(word_t a, word_t b, bit_t s, word_t *f);
void Mux4To116(word_t a, word_t b, word_t c, word_t d, bit_t s[2], word_t *f);

/* Adder functions */
void HalfAdder(bit_t a, bit_t b, bit_t *s, bit_t *cout);
void FullAdder(bit_t a, bit_t b, bit_t cin, bit_t *s, bit_t *cout);

/* 16-bit Adder functions */
void Adder16(word_t a, word_t b, bit_t cin, word_t *s, bit_t *cout);

/* 16-bit ALU functions */
void ALU16(word_t a, word_t b, struct alu_ctl_t ctl, word_t *out, bit_t *zr, bit_t *ng);

/* Clock functions */
bit_t ClockCycle(void);

/* DFF functions */
bit_t DFFCycle(bit_t d, bit_t clk, bit_t in);

/* Register functions */
bit_t RegisterCycle(bit_t r, bit_t clk, bit_t in, bit_t load);

/* 16-bit Register functions */
word_t Register16Cycle(word_t r, bit_t clk, word_t in, bit_t load);

/* 16-bit Memory functions */
word_t Memory16Cycle(word_t *m, bit_t clk, word_t in, word_t addr, bit_t load);

/* 16-bit Counter functions */
word_t Counter16Cycle(word_t c, bit_t clk, bit_t inc, bit_t load, bit_t reset, word_t in);

#endif

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
