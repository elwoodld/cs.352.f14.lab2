/* util.c: Utility functions */

#include "sam16.h"

#include <string.h>

/**
 * Convert word to binary string.
 *
 * @param w Input word.
 * @param s Output binary string.
 */
void word_to_binary_string(word_t w, char *s) {
    
    for(int i = 0; i < WORD_SIZE; i++){
        s[WORD_SIZE - 1 - i] = GET_BIT(w, i) + '0';
    }
   s[WORD_SIZE] = '\0';
    
    
}

/**
 * Convert binary string to word.
 *
 * @param s Input binary string.
 * @param w Output word.
 */
void binary_string_to_word(char *s, word_t *w) {
    // TODO: Convert up to WORD_SIZE characters
    size_t len = strlen(s);
    // TODO: Initialize w to 0
     *w = 0;
    // TODO: Set each bit in w to the corresponding value in s
    for(int i = 0; i < len; i++){
        SET_BIT(*w,len - 1 - i, s[i] - '0');
    }
}

/**
 * bits to word: converts bit array to word
 *
 * @param b Input bit array.
 * @param w Output word.
 */
void bits_to_word(bit_t b[WORD_SIZE], word_t *w) {
    // TODO: Initialize w to 0
    *w = 0;
    // TODO: Set each bit in word to match bit array
    for(int i = 0; i < WORD_SIZE; i++){
        SET_BIT(*w,(WORD_SIZE-1-i), b[WORD_SIZE-1-i]);
    }
}

/**
 * reverse bits: reverses bit array.
 *
 * @param b Input bit array.
 */
void reverse_bits(bit_t b[WORD_SIZE]) {
    // TODO: swap the bits in the array
    bit_t replacer[WORD_SIZE];

    for(int i = 0; i < WORD_SIZE; i++){
       replacer[i] =b[WORD_SIZE - 1 - i];
       
    }
    for(int i = 0; i < WORD_SIZE; i++){
        b[i] = replacer[i];
    }
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
