/* alu.c: ALU interpreter */

#include "sam16.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
    char buffer[BUFSIZ];
    int  ncommand = 0;

    while (true) {
        printf("ALU[%4d]> ", ncommand++);

        if (!fgets(buffer, BUFSIZ, stdin)) {
            break;
        }

        // TODO: Parse buffer, compute ALU expression, print output
    }

    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
