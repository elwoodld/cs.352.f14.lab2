/* timer.c: Timer Chip */

#include "sam16.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/**
 * Timer Controller structure
 */
struct timer_t {
    word_t seconds;
    word_t minutes;
    bit_t  s_inc;
    bit_t  s_reset;
    bit_t  s_load;
    bit_t  m_inc;
    bit_t  m_reset;
    bit_t  m_load;
};

/**
 * Tests if two words are equal.
 *
 * This uses logic gates to determine if w0 and w1 are equal.  The output bit f
 * is set to 1 if the two words or equal, otherwise 0.
 *
 * @param w0 Input word.
 * @param w1 Input word.
 * @param f  Output bit.
 */
void Equal16(word_t w0, word_t w1, bit_t *f) {
    // TODO: Implement using logic gates
    word_t holder;
    Xor16(w0,w1,&holder);
    Or16Way(holder,f);
    Not(*f,f);
}

/**
 * Updates Timer control signals.
 *
 * This uses wiring (i.e. assignment) and logic gates to update the Timer
 * control signals so that the seconds and minutes counters go from 00 - 59
 * (and roll over after that).
 *
 * @param timer Timer Controller struct.
 */
void TimerController(struct timer_t *timer) {
    // TODO: Set timer seconds control signals using logic gates
        bit_t sec;
        Equal16((*timer).seconds,59,&sec);

        (*timer).s_inc = 1;
        (*timer).s_load = 0;
        (*timer).s_reset = sec;

    // TODO: Set timer minutes control signals using logic gates
        bit_t min;
        Equal16((*timer).minutes,59,&min);
        
        (*timer).m_inc = sec;
        (*timer).m_load = 0;

        if(min == 1 && sec == 1){
            (*timer).m_reset = 1;
        }else{
            (*timer).m_reset = 0;
        }


}

int main(int argc, char *argv[]) {
    struct timer_t timer;
    bit_t clk = 0;
    
        
    while (true) {
        // TODO: Upate timer controller struct signals
        TimerController(&timer);
        // TODO: Update clock, seconds counter, and minutes counter
        if(clk == 0){
            clk = 1;
        }
       
        timer.seconds = Counter16Cycle(timer.seconds,clk,timer.s_inc,timer.s_load,timer.s_reset,argc);
        timer.minutes = Counter16Cycle(timer.minutes,clk,timer.m_inc,timer.m_load,timer.m_reset,argc);
            
        
        if (clk) {
            if (getenv("TIMER_DEBUG")) {
                printf("Time: %02d:%02d\n", timer.minutes, timer.seconds);
            } else {
                printf("\rTime: %02d:%02d", timer.minutes, timer.seconds);
                fflush(stdout);
                sleep(1);
            }
        }
    }

    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
