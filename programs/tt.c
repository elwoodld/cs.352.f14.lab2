/* tt.c: Boolean Chip based on Truth Table */

#include "sam16.h"

#include <stdio.h>
#include <stdlib.h>

/**
 * Implements Logic Gate specified by Truth Table:
 *
 *  a | b | c | d || f
 *  ------------------
 *  0 | 0 | 0 | 0 || 0
 *  0 | 0 | 0 | 1 || 1
 *  0 | 0 | 1 | 0 || 0
 *  0 | 0 | 1 | 1 || 1
 *  0 | 1 | 0 | 0 || 0
 *  0 | 1 | 0 | 1 || 1
 *  0 | 1 | 1 | 0 || 1
 *  0 | 1 | 1 | 1 || 0
 *  1 | 0 | 0 | 0 || 0
 *  1 | 0 | 0 | 1 || 1
 *  1 | 0 | 1 | 0 || 0
 *  1 | 0 | 1 | 1 || 1
 *  1 | 1 | 0 | 0 || 1
 *  1 | 1 | 0 | 1 || 1
 *  1 | 1 | 1 | 0 || 0
 *  1 | 1 | 1 | 1 || 1
 *
 * The implementation should be minimized using Algebraic Laws or Karnaugh
 * maps.
 *
 * @param a Input bit.
 * @param b Input bit.
 * @param c Input bit.
 * @param d Input bit.
 * @param f Output bit.
 */
void TTGate(bit_t a, bit_t b, bit_t c, bit_t d, bit_t *f) {
    // TODO: Implement using 1-bit logic gates
    bit_t abcd;
    bit_t abc;
    bit_t bd;
    bit_t cd;
    bit_t ad;
    bit_t not;
    
    Not(a,&not);
    And(not,b,&abcd);
    And(abcd,c,&abcd);
    Not(d,&not);
    And(abcd,not,&abcd);
    //first of 5 compolete
    And(a,b,&abc);
    Not(c,&not);
    And(abc,not,&abc);
    //second of 5 complete
    And(not,d,&cd);
    //thried of 5 complete
    Not(b,&not);
    And(not,d,&bd);
    //fourth complete
    And(a,d,&ad);
    //fifth complete
    Or(abcd,abc,f);
    Or(*f,bd,f);
    Or(*f,cd,f);
    Or(*f,ad,f);
    
}

int main(int argc, char *argv[]) {
    bit_t f;

    printf("%2s | %2s | %2s | %2s || %2s\n", "a", "b", "c", "d", "f");

    for (int a = 0; a < 2; a++) {
        for (int b = 0; b < 2; b++) {
            for (int c = 0; c < 2; c++) {
                for (int d = 0; d < 2; d++) {
                    TTGate((bit_t)a, (bit_t)b, (bit_t)c, (bit_t)d, &f);
                    printf("%2u | %2u | %2u | %2u || %2u\n", a, b, c, d, f);
                }
            }
        }
    }

    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
