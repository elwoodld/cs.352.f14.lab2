/* test_adder.c: Adder functions test */

#include "sam16.h"

#include <stdio.h>
#include <stdlib.h>

void TestHalfAdder() {
    bit_t s;
    bit_t cout;

    printf("HalfAdder:\n");
    printf("%4s | %4s || %4s | %4s\n", "a", "b", "s", "cout");

    for (int a = 0; a < 2; a++) {
        for (int b = 0; b < 2; b++) {
            HalfAdder((bit_t)a, (bit_t)b, &s, &cout);
            printf("%4u | %4u || %4u | %4u\n", a, b, s, cout);
        }
    }
}

void TestFullAdder() {
    bit_t s;
    bit_t cout;

    printf("FullAdder:\n");
    printf("%4s | %4s | %4s || %4s | %4s\n", "a", "b", "cin", "s", "cout");

    for (int cin = 0; cin < 2; cin++) {
        for (int a = 0; a < 2; a++) {
            for (int b = 0; b < 2; b++) {
                FullAdder((bit_t)a, (bit_t)b, (bit_t)cin, &s, &cout);
                printf("%4u | %4u | %4u || %4u | %4u\n", a, b, cin, s, cout);
            }
        }
    }
}

int main(int argc, char *argv[]) {
    TestHalfAdder();
    TestFullAdder();

    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
