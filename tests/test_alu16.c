/* TestALU16.c: ALU functions test */

#include "sam16.h"

#include <stdio.h>
#include <stdlib.h>

void TestALU16(word_t x, word_t y, struct alu_ctl_t ctl) {
    char xs[BS_LEN];
    char ys[BS_LEN];
    char os[BS_LEN];
    word_t out;
    bit_t zr;
    bit_t ng;

    ALU16(x, y, ctl, &out, &zr, &ng);
    word_to_binary_string(x, xs);
    word_to_binary_string(y, ys);
    word_to_binary_string(out, os);

    printf("%*s | %*s | %2s | %2s | %2s | %2s | %2s | %2s || %*s | %2s | %2s\n",
            WORD_SIZE, "x",
            WORD_SIZE, "y",
            "zx",
            "nx",
            "zy",
            "ny",
            "f",
            "no",
            WORD_SIZE, "out",
            "zr",
            "ng"
    );
    printf("%*s | %*s | %2u | %2u | %2u | %2u | %2u | %2u || %*s | %2u | %2u\n\n",
            WORD_SIZE, xs,
            WORD_SIZE, ys,
            ctl.zx,
            ctl.nx,
            ctl.zy,
            ctl.ny,
            ctl.f,
            ctl.no,
            WORD_SIZE, os,
            zr,
            ng
    );
}

int main(int argc, char *argv[]) {
    struct alu_ctl_t ctl = {0};

    /* 0 */
    printf("= 0 =\n");
    ctl.zx = 1;
    ctl.nx = 0;
    ctl.zy = 1;
    ctl.ny = 0;
    ctl.f  = ALU_ADD;
    ctl.no = 0;
    TestALU16(1, 1, ctl);

    /* 1 */
    printf("= 1 =\n");
    ctl.zx = 1;
    ctl.nx = 1;
    ctl.zy = 1;
    ctl.ny = 1;
    ctl.f  = ALU_ADD;
    ctl.no = 1;
    TestALU16(0, 0, ctl);

    /* -1 */
    printf("= -1 =\n");
    ctl.zx = 1;
    ctl.nx = 1;
    ctl.zy = 1;
    ctl.ny = 0;
    ctl.f  = ALU_ADD;
    ctl.no = 0;
    TestALU16(0, 0, ctl);

    /* x */
    printf("= x =\n");
    ctl.zx = 0;
    ctl.nx = 0;
    ctl.zy = 1;
    ctl.ny = 1;
    ctl.f  = ALU_AND;
    ctl.no = 0;
    TestALU16(1, 0, ctl);
    TestALU16(0, 1, ctl);

    /* y */
    printf("= y =\n");
    ctl.zx = 1;
    ctl.nx = 1;
    ctl.zy = 0;
    ctl.ny = 0;
    ctl.f  = ALU_AND;
    ctl.no = 0;
    TestALU16(1, 0, ctl);
    TestALU16(0, 1, ctl);

    /* !x */
    printf("= !x =\n");
    ctl.zx = 0;
    ctl.nx = 0;
    ctl.zy = 1;
    ctl.ny = 1;
    ctl.f  = ALU_AND;
    ctl.no = 1;
    TestALU16(1, 0, ctl);
    TestALU16(0, 1, ctl);

    /* !y */
    printf("= !y =\n");
    ctl.zx = 1;
    ctl.nx = 1;
    ctl.zy = 0;
    ctl.ny = 0;
    ctl.f  = ALU_AND;
    ctl.no = 1;
    TestALU16(1, 0, ctl);
    TestALU16(0, 1, ctl);

    /* -x */
    printf("= -x =\n");
    ctl.zx = 0;
    ctl.nx = 0;
    ctl.zy = 1;
    ctl.ny = 1;
    ctl.f  = ALU_ADD;
    ctl.no = 1;
    TestALU16(1, 0, ctl);

    /* -y */
    printf("= -y =\n");
    ctl.zx = 1;
    ctl.nx = 1;
    ctl.zy = 0;
    ctl.ny = 0;
    ctl.f  = ALU_ADD;
    ctl.no = 1;
    TestALU16(0, 1, ctl);

    /* x + 1 */
    printf("= x + 1 =\n");
    ctl.zx = 0;
    ctl.nx = 1;
    ctl.zy = 1;
    ctl.ny = 1;
    ctl.f  = ALU_ADD;
    ctl.no = 1;
    TestALU16(0, 0, ctl);

    /* y + 1 */
    printf("= y + 1 =\n");
    ctl.zx = 1;
    ctl.nx = 1;
    ctl.zy = 0;
    ctl.ny = 1;
    ctl.f  = ALU_ADD;
    ctl.no = 1;
    TestALU16(0, 0, ctl);

    /* x - 1 */
    printf("= x - 1 =\n");
    ctl.zx = 0;
    ctl.nx = 0;
    ctl.zy = 1;
    ctl.ny = 1;
    ctl.f  = ALU_ADD;
    ctl.no = 0;
    TestALU16(0, 0, ctl);

    /* y - 1 */
    printf("= y - 1 =\n");
    ctl.zx = 1;
    ctl.nx = 1;
    ctl.zy = 0;
    ctl.ny = 0;
    ctl.f  = ALU_ADD;
    ctl.no = 0;
    TestALU16(0, 0, ctl);

    /* x + y */
    printf("= x + y =\n");
    ctl.zx = 0;
    ctl.nx = 0;
    ctl.zy = 0;
    ctl.ny = 0;
    ctl.f  = ALU_ADD;
    ctl.no = 0;
    TestALU16(0, 0, ctl);
    TestALU16(0, 1, ctl);
    TestALU16(1, 0, ctl);
    TestALU16(1, 1, ctl);

    /* x - y */
    printf("= x - y =\n");
    ctl.zx = 0;
    ctl.nx = 1;
    ctl.zy = 0;
    ctl.ny = 0;
    ctl.f  = ALU_ADD;
    ctl.no = 1;
    TestALU16(0, 0, ctl);
    TestALU16(0, 1, ctl);
    TestALU16(1, 0, ctl);
    TestALU16(1, 1, ctl);

    /* y - x */
    printf("= y - x =\n");
    ctl.zx = 0;
    ctl.nx = 0;
    ctl.zy = 0;
    ctl.ny = 1;
    ctl.f  = ALU_ADD;
    ctl.no = 1;
    TestALU16(0, 0, ctl);
    TestALU16(0, 1, ctl);
    TestALU16(1, 0, ctl);
    TestALU16(1, 1, ctl);

    /* x & y */
    printf("= x & y =\n");
    ctl.zx = 0;
    ctl.nx = 0;
    ctl.zy = 0;
    ctl.ny = 0;
    ctl.f  = ALU_AND;
    ctl.no = 0;
    TestALU16(0, 0, ctl);
    TestALU16(0, 1, ctl);
    TestALU16(1, 0, ctl);
    TestALU16(1, 1, ctl);

    /* x | y */
    printf("= x | y =\n");
    ctl.zx = 0;
    ctl.nx = 1;
    ctl.zy = 0;
    ctl.ny = 1;
    ctl.f  = ALU_AND;
    ctl.no = 1;
    TestALU16(0, 0, ctl);
    TestALU16(0, 1, ctl);
    TestALU16(1, 0, ctl);
    TestALU16(1, 1, ctl);

    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
