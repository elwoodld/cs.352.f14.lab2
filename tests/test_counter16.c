/* test_counter.c: counter functions test */

#include "sam16.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

word_t TestCounter16(word_t count, bit_t s_inc, bit_t s_load, bit_t s_reset, word_t in) {
    bit_t clk;

    for (int i = 0; i < 2; i++) {
        clk   = ClockCycle();
        count = Counter16Cycle(count, clk, s_inc, s_load, s_reset, in);

        printf("count=%d, clk=%d, s_inc=%d, s_load=%d, s_reset=%d, in=%d\n",
                count,
                clk,
                s_inc,
                s_load,
                s_reset,
                in);
    }

    return count;
}

int main(int argc, char *argv[]) {
    word_t count   = 0;

    puts("= Do nothing =");
    count = TestCounter16(count, 0, 0, 0, UWORD_MAX);
    count = TestCounter16(count, 0, 0, 0, UWORD_MAX);

    puts("= Increment by 1 =");
    count = TestCounter16(count, 1, 0, 0, UWORD_MAX);
    count = TestCounter16(count, 1, 0, 0, UWORD_MAX);

    puts("= Load UWORD_MAX =");
    count = TestCounter16(count, 1, 1, 0, UWORD_MAX);
    count = TestCounter16(count, 1, 1, 0, UWORD_MAX);

    puts("= Increment by 1 =");
    count = TestCounter16(count, 1, 0, 0, UWORD_MAX);
    count = TestCounter16(count, 1, 0, 0, UWORD_MAX);

    puts("= Load UWORD_MAX =");
    count = TestCounter16(count, 0, 1, 0, UWORD_MAX);
    count = TestCounter16(count, 0, 1, 0, UWORD_MAX);

    puts("= Reset =");
    count = TestCounter16(count, 0, 1, 1, UWORD_MAX);
    count = TestCounter16(count, 0, 1, 1, UWORD_MAX);

    puts("= Increment by 1 =");
    count = TestCounter16(count, 1, 0, 0, UWORD_MAX);
    count = TestCounter16(count, 1, 0, 0, UWORD_MAX);

    puts("= Reset =");
    count = TestCounter16(count, 1, 1, 1, UWORD_MAX);
    count = TestCounter16(count, 1, 1, 1, UWORD_MAX);

    puts("= Increment by 1 =");
    count = TestCounter16(count, 1, 0, 0, UWORD_MAX);
    count = TestCounter16(count, 1, 0, 0, UWORD_MAX);

    puts("= Reset =");
    count = TestCounter16(count, 1, 0, 1, UWORD_MAX);
    count = TestCounter16(count, 1, 0, 1, UWORD_MAX);

    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
