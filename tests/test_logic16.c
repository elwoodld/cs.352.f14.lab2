/* test_logic16.c: 16-bit Logic functions test */

#include "sam16.h"

#include <stdio.h>
#include <stdlib.h>

#define TEST_MAX 3

typedef void word_logic_func_t(word_t, word_t, word_t *);
typedef void way_logic_func_t(word_t, bit_t *);
typedef void word_mux2to1_func_t(word_t, word_t, bit_t, word_t *);
typedef void word_mux4to1_func_t(word_t, word_t, word_t, word_t, bit_t[2], word_t *);

void TestWordLogicFunc(word_logic_func_t *f, const char *s) {
    word_t c;
    char as[BS_LEN];
    char bs[BS_LEN];
    char cs[BS_LEN];

    printf("%s:\n", s);
    printf("%*s | %*s || %*s\n", WORD_SIZE, "a", WORD_SIZE, "b", WORD_SIZE, "f");

    for (int a = 0; a < TEST_MAX; a++) {
        for (int b = 0; b < TEST_MAX; b++) {
            f((word_t)a, (word_t)b, &c);

            word_to_binary_string(a, as);
            word_to_binary_string(b, bs);
            word_to_binary_string(c, cs);

            printf("%*s | %*s || %*s\n", WORD_SIZE, as, WORD_SIZE, bs, WORD_SIZE, cs);
        }
    }
}

void TestWayLogicFunc(way_logic_func_t *func, const char *s) {
    word_t words[] = {0, 1, 2, 3, 4, UWORD_MAX, WORD_MAX, WORD_MIN};
    bit_t f;
    char ws[BS_LEN];

    printf("%s:\n", s);
    printf("%*s || %s\n", WORD_SIZE, "w", "f");

    for (int w = 0; w < sizeof(words)/sizeof(word_t); w++) {
        func((word_t)words[w], &f);
        word_to_binary_string(words[w], ws);
        printf("%*s || %d\n", WORD_SIZE, ws, f);
    }
}

void TestWordMux2To1Func(word_mux2to1_func_t *f, const char *s) {
    word_t a = 0;
    word_t b = 1;
    word_t c;
    char as[BS_LEN];
    char bs[BS_LEN];
    char cs[BS_LEN];

    printf("%s:\n", s);
    printf("%*s | %*s | %2s || %*s\n", WORD_SIZE, "a", WORD_SIZE, "b", "s", WORD_SIZE, "f");

    for (int s = 0; s < 2; s++) {
        f((word_t)a, (word_t)b, (bit_t)s, &c);

        word_to_binary_string(a, as);
        word_to_binary_string(b, bs);
        word_to_binary_string(c, cs);

        printf("%*s | %*s | %2u || %*s\n", WORD_SIZE, as, WORD_SIZE, bs, s, WORD_SIZE, cs);
    }
}

void TestWordMux4To1Func(word_mux4to1_func_t *f, const char *s) {
    word_t a = 0;
    word_t b = 1;
    word_t c = 2;
    word_t d = 3;
    word_t e;
    char as[BS_LEN];
    char bs[BS_LEN];
    char cs[BS_LEN];
    char ds[BS_LEN];
    char es[BS_LEN];

    printf("%s:\n", s);
    printf("%*s | %*s | %*s | %*s | %2s || %*s\n", WORD_SIZE, "a", WORD_SIZE, "b", WORD_SIZE, "c", WORD_SIZE, "d", "s", WORD_SIZE, "f");

    for (int s1 = 0; s1 < 2; s1++) {
        for (int s0 = 0; s0 < 2; s0++) {
            bit_t sb[2] = {s1, s0};

            f(a, b, c, d, sb, &e);

            word_to_binary_string(a, as);
            word_to_binary_string(b, bs);
            word_to_binary_string(c, cs);
            word_to_binary_string(d, ds);
            word_to_binary_string(e, es);

            printf("%*s | %*s | %*s | %*s | %u%u || %*s\n", WORD_SIZE, as, WORD_SIZE, bs, WORD_SIZE, cs, WORD_SIZE, ds, s1, s0, WORD_SIZE, es);
        }

    }
}

int main(int argc, char *argv[]) {
    TestWordLogicFunc(Nand16, "Nand16");
    TestWordLogicFunc( And16, "And16");
    TestWordLogicFunc(  Or16, "Or16");
    TestWordLogicFunc( Xor16, "Xor16");

    TestWayLogicFunc(Or16Way, "Or16Way");

    TestWordMux2To1Func(Mux2To116, "Mux2To116");
    TestWordMux4To1Func(Mux4To116, "Mux4To116");

    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
