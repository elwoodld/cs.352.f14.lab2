/* test_memory16.c: 16-bit memory functions test */

#include "sam16.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LIMIT 12
#define MEMORY_SIZE 64

const bit_t LOAD[LIMIT] = {
    0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1
};

int main(int argc, char *argv[]) {
    word_t m[MEMORY_SIZE];
    word_t addr = 0;
    bit_t  clk  = ClockCycle();

    memset(m, 0, sizeof(word_t)*MEMORY_SIZE);

    for (word_t i = 0; i < LIMIT; i++) {
        m[addr] = Memory16Cycle(m, clk, i, addr, LOAD[i]);
        printf("m[%d]=%2u, clk=%2d, in=%2d, load=%2d\n", addr, m[addr], clk, i, LOAD[i]);
        clk = ClockCycle();
        if (i % 2) {
            addr = (addr + 1) % 2;
        }
    }

    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
