/* test_util.c: Utility functions test */

#include "sam16.h"

#include <stdio.h>
#include <stdlib.h>

void TestUtilBSW(word_t w) {
    char bs[BS_LEN];

    do {
        word_to_binary_string(w, bs);
        binary_string_to_word(bs, &w);
        printf("%*s %6u %6d\n", WORD_SIZE, bs, w, (int16_t)w);
    } while (w <<= 1);
}

void TestUtilBits() {
    bit_t b[WORD_SIZE] = {1};
    word_t w;
    char bs[BS_LEN];

    bits_to_word(b, &w);
    word_to_binary_string(w, bs);
    printf("%*s %6u %6d\n", WORD_SIZE, bs, w, (int16_t)w);

    reverse_bits(b);
    bits_to_word(b, &w);
    word_to_binary_string(w, bs);
    printf("%*s %6u %6d\n", WORD_SIZE, bs, w, (int16_t)w);
}

int main(int argc, char *argv[]) {
    TestUtilBSW(1);
    TestUtilBSW(UWORD_MAX);
    TestUtilBits();

    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
